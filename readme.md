# Install Jira Software Server on CentOS 7

Put vault password in file vault_password.txt

##### Configure ssh key access
Client: `$ ssh-keygen -f ~/.ssh/id_rsa_centos` 

CentOS server: Append public key from `~/.ssh/id_rsa_centos.pub` (client) to file `~/.ssh/authorized_keys` (server)

Client: Fill file `~/.ssh/config`
```
$ cat ~/.ssh/config
Host c7
    Hostname <centos_server_address>
    IdentityFile ~/.ssh/id_rsa_centos
    IdentitiesOnly yes
```

Run installation:
```
ansible-playbook -i ./inventories/ --vault-id=dev@vault_password.txt ./playbooks/jira_centos.yml
```

This playbook will install and configure PostgreSQL, than download and install Jira Software server.

Jira server address:
```
http://<centos_server_address>:8080
```